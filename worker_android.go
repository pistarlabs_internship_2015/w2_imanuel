//BEANSTALKD ANDROID CONSUMER

package main
import (
    "github.com/iwanbk/gobeanstalk"
    "log"
	"strings"
    "fmt"
    "github.com/googollee/go-gcm"
)

func main() {
    conn, err := gobeanstalk.Dial("192.168.0.45:11300")
    if err != nil {
        log.Fatal(err)
    }
    for {
        conn.Watch("gcm")
		j, err := conn.Reserve()
        if err != nil {
            log.Fatal(err)
        }
        log.Printf("id:%d, body:%s\n", j.ID, string(j.Body))
		
		//SPLIT DATA
		result := strings.Split(string(j.Body), "#####CONCAT#####")
		// Display all elements.
		for i := range result {
			fmt.Println("result #",i, result[i])
		}
		// Length
		fmt.Println(len(result))
				
		//PUSH NOTIFICATION PROCESS//
		pushToAndroid("AIzaSyCoWoVdYNs-Jqr02CltCbCDq19mHiElGzk",result[0])
		fmt.Println("andro push : success")
	
        err = conn.Delete(j.ID) 
        if err != nil {
            log.Fatal(err)
        }
    }
}

func pushToAndroid(app_key string,device_token string){
    client := gcm.New(app_key)

    load := gcm.NewMessage(device_token)
    load.AddRecipient("abc")
    load.SetPayload("data", "1")
    load.CollapseKey = "demo"
    load.DelayWhileIdle = true
    load.TimeToLive = 10

    resp, err := client.Send(load)

    fmt.Printf("id: %+v\n", resp)
    fmt.Println("err:", err)
    fmt.Println("err index:", resp.ErrorIndexes())
    fmt.Println("reg index:", resp.RefreshIndexes())
}