//BEANSTALKD IOS CONSUMER

package main
import (
    "github.com/iwanbk/gobeanstalk"
    "log"
	"strings"
    "fmt"
	apns "github.com/anachronistic/apns"
    "gopkg.in/mgo.v2"
    "gopkg.in/mgo.v2/bson"
    //"io/ioutil"; 
)

type Applications struct {
    ID bson.ObjectId `bson:"_id,omitempty"`
    Name string `bson:"name"`
    App_Version string `bson:"app_version"`
    Key_Access string `bson:"key_access"`
    GCM_Access_Key string `bson:"gcm_access_key"`
    Baidu_Access_Key string `bson:"baidu_access_key"`
    APNS_PEM string `bson:"apns_pem"`
    APNS_PEM_NoEnc string `bson:"apns_pem_noenc"`
}

func main() {
    conn, err := gobeanstalk.Dial("192.168.0.45:11300")
    if err != nil {
        log.Fatal(err)
    }
    for {
		conn.Watch("apns")
        j, err := conn.Reserve()
        if err != nil {
            log.Fatal(err)
        }
        log.Printf("id:%d, body:%s\n", j.ID, string(j.Body))
		
		//SPLIT DATA
		result := strings.Split(string(j.Body), "#####CONCAT#####")
		// Display all elements.
		for i := range result {
			fmt.Println("result #",i, result[i])
		}
		// Length
		fmt.Println(len(result))
		
		//PUSH NOTIFICATION PROCESS//
        apns_pem, apns_pem_noenc:=FindAPNSKey()
		pushtoIOS(apns_pem,apns_pem_noenc,result[0],result[1],result[2])	
		
        err = conn.Delete(j.ID) 
        if err != nil {
            log.Fatal(err)
        }
    }
}

func pushtoIOS(apns_pem string, apns_pem_noenc string, device_token string, message string, push_code string){
	payload := apns.NewPayload()
    payload.Alert = message
    payload.Badge = 1
    payload.Sound = "default"

    pn := apns.NewPushNotification()
    //pn.DeviceToken = "ff5407377561851dfd83d4e29a4f03b51b856edfd55b94329370ab4d1f177cdc";
    pn.DeviceToken = device_token

    pn.AddPayload(payload)
    client := apns.NewClient("gateway.sandbox.push.apple.com:2195", apns_pem, apns_pem_noenc)
    resp := client.Send(pn)

    alert, _ := pn.PayloadString()
    fmt.Println("Alert:", alert)
    fmt.Println("Success", resp.Success)
    fmt.Println("Error", resp.Error)

    if(resp.Success){
        fmt.Println("ios push : success")
        ChangeStatus(push_code)   
    }else{
        fmt.Println("ios push : failed ", resp.Error)
    }
}

func FindAPNSKey() (string,string){
    session, err := mgo.Dial("localhost:27017")
    if err != nil {
        panic(err)
    }

    defer session.Close()

    // Optional. Switch the session to a monotonic behavior.
    session.SetMode(mgo.Monotonic, true)

    //convenient access
    c := session.DB("dummydata2").C("applications")

    // Query One
    result := Applications{}
    err = c.Find(bson.M{"name": "Dyned Live"}).One(&result)
    if err != nil {
        panic(err)
    }
    //fmt.Println("apns pem method ",result.APNS_PEM)
    return result.APNS_PEM, result.APNS_PEM_NoEnc
}

func ChangeStatus(Push_Code string) {
    session, err := mgo.Dial("localhost:27017")
    if err != nil {
        panic(err)
    }

    defer session.Close()

    // Optional. Switch the session to a monotonic behavior.
    session.SetMode(mgo.Monotonic, true)

    //convenient access
    c := session.DB("dummydata2").C("logs")
    
    //update
    condition := bson.M{"push_code": Push_Code}
    change := bson.M{"$set": bson.M{"status": "pushed"}}
    err = c.Update(condition, change)
    if err != nil {
        log.Fatal(err)
    }
}