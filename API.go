//API FILE

package main

import(
	"encoding/json"
	"fmt"
	"net/http"
 	"github.com/iwanbk/gobeanstalk"
    "log"
    "time"
    "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Requests struct {
    Key_Access string
    User_ID string
    Message string
    App_ID string
    Device_ID string
}

type Users struct {
    _ID string `bson:"id"`
    User_ID string `bson:"user_id"`
    App []*App `bson:"app"`
}

type App struct {
    App_ID string `bson:"app_id"`
    App_Version string `bson:"app_version"`
    Devices []*Devices `bson:"devices"`
}

type Devices struct {
    Push_Type string `bson:"push_type"`
    Device_Model string `bson:"device_model"`
    Device_OS string `bson:"device_os"`
    Device_ID string `bson:"device_id"`
}

type Logs struct{
    App_ID string `bson:"app_id"`
    User_ID string `bson:"user_id"`
    Device_ID string `bson:"device_id"`
    Date_Access string `bson:"date_access"`
    Message string `bson:"message"`
    IP_Address string `bson:"ip_address"`
    Status string `bson:"status"`
}

var(
    push_type [1000]string 
    device_model [1000]string 
    device_os [1000]string 
    device_id [1000]string 
    num int
)

const(
	KEY_ACCESS ="1234567890"
)

type Response struct {
    Status string
}

func main() {
    http.HandleFunc("/",serveRest)
    http.HandleFunc("/push",Push)
    http.ListenAndServe("192.168.0.24:1300",nil)
}

func serveRest(w http.ResponseWriter, r *http.Request) {
	//VALIDATION//

	//CHECK KEY ACCESS
	if(KEY_ACCESS==r.FormValue("key_access")){
		//KEY ACCESS VALID
        fmt.Fprintf(w,"Your request has been sent")   
		//GET DATA REQUEST
		Req := Requests{User_ID:r.FormValue("user_id"), Message:r.FormValue("message")}
		//fmt.Fprintf(w,Req.User_ID)
        //fmt.Fprintf(w,Req.Message)

		//GET USER DEVICES
		SelectAllDevices(Req.User_ID,"","")

		//RECORD TO MONGO DB & WRITE TO BEANSTALKD
        for i := 0; i < num ;i++ {
            RecordToLogs("appid",Req.User_ID,device_id[i],Req.Message)
            //CONCATE ALL DATA
			data := []byte(device_id[i]+"#####CONCAT#####"+Req.Message)
			WriteToBeanstalkd(data, push_type[i])
        }  
		//
	}else{
		//KEY ACCESS FALSE
		fmt.Fprintf(w,"Key Access Wrong")	
	}
}

func delay(sec int) {
    for i := 0; i < sec; i++ {
        time.Sleep(1000 * time.Millisecond)
        //fmt.Println(s)
    }
}
func Push(w http.ResponseWriter, r *http.Request) {

    fmt.Fprintf(w, "Welcome\n")


    //GET DATA FROM TESTER
    decoder := json.NewDecoder(r.Body)
    var req Requests   
    err := decoder.Decode(&req)
    if err != nil {
        log.Fatal(err)
    }
    log.Println(req.Key_Access)
    log.Println(req.User_ID)

    //VALIDATION//

    //CHECK KEY ACCESS
    if(KEY_ACCESS==req.Key_Access){
        //delay(5)
        //RESPON TO TESTER
        profile := Response{"Success"}

        js, err := json.Marshal(profile)
        if err != nil {
            http.Error(w, err.Error(), http.StatusInternalServerError)
            return
        }

        w.Header().Set("Content-Type", "application/json")
        w.Write(js)


        //KEY ACCESS VALID
        fmt.Fprintf(w,"Your request has been sent")   
        //GET DATA REQUEST
        Req := Requests{User_ID:req.User_ID, Message:req.Message}
        //fmt.Fprintf(w,Req.User_ID)
        //fmt.Fprintf(w,Req.Message)

        //GET USER DEVICES
        SelectAllDevices(req.User_ID,"","")

        //RECORD TO MONGO DB & WRITE TO BEANSTALKD
        for i := 0; i < num ;i++ {
            RecordToLogs("appid",Req.User_ID,device_id[i],Req.Message)
            //CONCATE ALL DATA
            data := []byte(device_id[i]+"#####CONCAT#####"+Req.Message)
            WriteToBeanstalkd(data, push_type[i])
        }  
        //
    }else{
        //KEY ACCESS FALSE
        fmt.Fprintf(w,"Key Access Wrong")   
        
        //RESPON TO TESTER
        profile := Response{"Failed"}

        js, err := json.Marshal(profile)
        if err != nil {
            http.Error(w, err.Error(), http.StatusInternalServerError)
            return
        }

        w.Header().Set("Content-Type", "application/json")
        w.Write(js)
    }
    
}

func WriteToBeanstalkd(data []byte, TUBE_NAME string) {
    //CONNECT TO BEANSTALKD SERVER
    conn, err := gobeanstalk.Dial("192.168.0.45:11300")
    if err != nil {
        log.Fatal(err)
    }
    //INSERT DATA
	conn.Use(TUBE_NAME)
    id, err := conn.Put(data, 0, 0, 0)
    if err != nil {
        log.Fatal(err)
    }
    log.Printf("Job id %d inserted\n", id)
}

func SelectAllDevices(User_ID string, App_ID string, Device_ID string) {
        //connecting to mongoDB
        session, err := mgo.Dial("localhost:27017")
        if err != nil {
                panic(err)
        }

        defer session.Close()

        // Optional. Switch the session to a monotonic behavior.
        session.SetMode(mgo.Monotonic, true)

        //convenient access
        c := session.DB("dummydata2").C("devices")
        
        if err != nil {
                log.Fatal(err)
        }

        result := Devices{}
        iter := c.Find(bson.M{"user_id":User_ID}).Iter()
       
        num=0
        for iter.Next(&result){
                fmt.Println("Result #", num)
                fmt.Println("Push Type   : ", result.Push_Type)
                fmt.Println("Device Model: ", result.Device_Model)
                fmt.Println("Device OS   : ", result.Device_OS)
                fmt.Println("Device ID   : ", result.Device_ID)
                
                push_type[num]=result.Push_Type
                device_model[num]=result.Device_Model
                device_os[num]=result.Device_OS
                device_id[num]=result.Device_ID

                num++
        }

        fmt.Println("Device ID   : ", device_id[1])
}

func RecordToLogs(App_ID string, User_ID string, Device_ID string, Message string) {
        //connecting to mongoDB
        session, err := mgo.Dial("localhost:27017")
        if err != nil {
                panic(err)
        }

        defer session.Close()

        // Optional. Switch the session to a monotonic behavior.
        session.SetMode(mgo.Monotonic, true)

        //convenient access
        c := session.DB("dummydata2").C("logs")
       
        err = c.Insert(&Logs{App_ID, User_ID, Device_ID, "10-11-2010", Message, "192.168.0.24","Waiting"})
        if err != nil {
                log.Fatal(err)
        }
}